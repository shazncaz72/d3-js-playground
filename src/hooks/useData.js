import { useEffect, useState } from "react";
import { csv, timeParse } from "d3";

const csvUrl =
  "https://gist.githubusercontent.com/carlevans72/9686ab976dd4b0f25a2bc40ba1089204/raw/2e32428b95e3daf2befbc5d5fa947fd31d138236/Scotland-Population-1972-2019.csv";

const csvUrl2 =
  "https://gist.githubusercontent.com/carlevans72/9cca954fa10110edda8223e5551154b4/raw/7f9e39d676d97964f123bd5bce43cb84c5df06e7/Norway-Scotland-Population-1972-2019.csv";

export const useData = (multi = false) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    const parseTime = timeParse("%Y");

    if (multi) {
      const row = (d) => {
        d.NorwayPopulationNum = +d.NorwayPopulation;
        d.ScotlandPopulationNum = +d.ScotlandPopulation;
        d.ParsedYear = parseTime(d.Year);
        return d;
      };

      csv(csvUrl2, row).then((d) => setData(d));
      return;
    }

    const row = (d) => {
      d.PopulationNum = +d.Population;
      d.ParsedYear = parseTime(d.Year);
      return d;
    };

    csv(csvUrl, row).then((d) => setData(d));
  }, [multi]);

  return data;
};
