import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  NavLink,
} from "react-router-dom";
import "./App.css";
import AnimatedGaugeContainer from "./components/AnimatedGauge/AnimatedGaugeContainer";
import GaugeContainer from "./components/Gauge/GaugeContainer";
import BarChart from "./components/BarChart/BarChart";
import { Menu, Message } from "semantic-ui-react";
import { LineChart } from "./components/LineChart/LineChart";
import { MultiLineChart } from "./components/MultiLineChart/MultiLineChart";
import { MultiLineChartWithTooltip } from "./components/MultiLineChartWithTooltip/MultiLineChartWithTooltip";

const App = () => {
  return (
    <Router>
      <nav className="nav">
        <Menu.Item as={NavLink} exact to="/">
          Home
        </Menu.Item>
        <Menu.Item as={NavLink} to="/animated-gauge">
          Animated Gauge
        </Menu.Item>
        <Menu.Item as={NavLink} to="/gauge">
          Gauge
        </Menu.Item>
        <Menu.Item as={NavLink} to="/bar-chart">
          Bar Chart
        </Menu.Item>
        <Menu.Item as={NavLink} to="/line-chart">
          Line Chart
        </Menu.Item>
        <Menu.Item as={NavLink} to="/multi-line-chart">
          Multi Line Chart
        </Menu.Item>
        <Menu.Item as={NavLink} to="/multi-line-chart-with-tooltip">
          Multi Line Chart with ToolTip
        </Menu.Item>
      </nav>
      <Switch>
        <Route path="/animated-gauge">
          <AnimatedGaugeContainer />
        </Route>
        <Route path="/gauge">
          <GaugeContainer />
        </Route>
        <Route path="/bar-chart">
          <BarChart />
        </Route>
        <Route path="/line-chart">
          <LineChart />
        </Route>
        <Route path="/multi-line-chart">
          <MultiLineChart />
        </Route>
        <Route path="/multi-line-chart-with-tooltip">
          <MultiLineChartWithTooltip />
        </Route>
        <Route exact path="/">
          <div className="container">
            <Message info>
              <Message.Header>
                Prototype Data Visualisations using D3
              </Message.Header>
              <p>Choose a Data Visualisation ...</p>
            </Message>
          </div>
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
