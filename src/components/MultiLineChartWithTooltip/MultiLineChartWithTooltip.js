import React, { useCallback, useEffect, useRef } from "react";
import { useData } from "../../hooks/useData";
import {
  axisBottom,
  axisLeft,
  extent,
  line,
  max,
  pointer,
  scaleTime,
  scaleLinear,
  select,
} from "d3";

const width = 960;
const height = 500;
// Margin convention
const margin = { top: 60, right: 50, bottom: 80, left: 120 };
const innerWidth = width - margin.left - margin.right;
const innerHeight = height - margin.top - margin.bottom;

export const MultiLineChartWithTooltip = () => {
  const svgRef = useRef();

  const data = useData(true);

  const createXScale = useCallback(() => {
    return scaleTime()
      .domain(extent(data, (d) => d.ParsedYear))
      .range([0, innerWidth]);
    // .nice();
  }, [data]);

  const createYScale = useCallback(() => {
    return scaleLinear()
      .domain([
        0,
        max(data, (d) =>
          Math.max(d.ScotlandPopulationNum, d.NorwayPopulationNum)
        ),
      ])
      .range([innerHeight, 0]);
    // .nice();
  }, [data]);

  const renderXAxis = (svg, xScale) => {
    const xAxis = axisBottom(xScale);

    svg
      .select(".x-axis")
      .style("transform", `translateY(${innerHeight}px)`)
      .call(xAxis)
      .selectAll("text")
      .attr("dy", "1.4em");
  };

  const renderYAxis = (svg, yScale) => {
    const yAxis = axisLeft(yScale);
    svg
      .select(".y-axis")
      .call(yAxis)
      .selectAll("line")
      .attr("x1", -6)
      .attr("x2", 8);
  };

  const renderAxisLabels = (chartArea, xAxisLabel, yAxisLabel) => {
    // X axis label
    chartArea
      .append("text")
      .attr("class", "x-axis-label")
      .attr("x", innerWidth / 2)
      .attr("y", innerHeight + 60)
      .text(xAxisLabel);

    // Y axis label
    chartArea
      .append("text")
      .attr("class", "y-axis-label")
      .attr("x", -90)
      .attr("y", innerHeight / 2)
      .attr("transform", `rotate(-90, ${-90}, ${innerHeight / 2})`)
      .text(yAxisLabel);
  };

  const renderYAxisGridLines = (chartArea, yScale) => {
    chartArea
      .selectAll("line.y-axis-grid")
      .data(yScale.ticks())
      .enter()
      .append("line")
      .attr("class", "y-axis-grid")
      .attr("x1", 18)
      .attr("x2", innerWidth)
      .attr("y1", (d) => yScale(d) + 1)
      .attr("y2", (d) => yScale(d) + 1)
      .attr("stroke", (d, idx) => {
        if (idx === 0) {
          return "none";
        }
        return "#e5e5e5";
      });
  };

  const renderValueLinePath = useCallback(
    (chartArea, className, valueLine, stroke) => {
      return chartArea
        .selectAll(`.${className}`)
        .data([data])
        .join("path")
        .attr("class", className)
        .attr("d", (value) => valueLine(value))
        .attr("fill", "none")
        .attr("stroke", stroke)
        .attr("stroke-width", 2);
    },
    [data]
  );

  const renderChart = useCallback(() => {
    const svg = select(svgRef.current);
    svg.attr("width", width).attr("height", height);

    // Margin convention
    const chartArea = svg
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);
    chartArea.append("g").attr("class", "x-axis axis");
    chartArea.append("g").attr("class", "y-axis axis");

    const xScale = createXScale();
    const yScale = createYScale();

    renderXAxis(svg, xScale);
    renderYAxis(svg, yScale);

    renderAxisLabels(chartArea, "Year", "Population");

    renderYAxisGridLines(chartArea, yScale);

    const valueLineScotland = line()
      .x((value) => xScale(value.ParsedYear))
      .y((value) => yScale(value.ScotlandPopulationNum));

    const valueLineNorway = line()
      .x((value) => xScale(value.ParsedYear))
      .y((value) => yScale(value.NorwayPopulationNum));

    // Path for line
    const pathScotland = renderValueLinePath(
      chartArea,
      "line-scotland",
      valueLineScotland,
      "#137880"
    );

    const pathNorway = renderValueLinePath(
      chartArea,
      "line-norway",
      valueLineNorway,
      "#5A7CD6"
    );

    const tooltip = select("body")
      .append("div")
      .attr("class", "tooltip")
      .style("position", "absolute")
      .style("opacity", 0);

    const handleMouseMove = (e) => {
      const [x] = pointer(e);
      const year = xScale.invert(x);
      const displayYear = year.getFullYear();

      tooltipLine
        .attr("stroke", "#CECECE")
        .attr("x1", `${xScale(year)}px`)
        .attr("x2", `${xScale(year)}px`)
        .attr("y1", 0)
        .attr("y2", innerHeight);

      tooltip
        .html(`<div style="color: black;">${displayYear}</div>`)
        .style("left", `${e.pageX + 20}px`)
        .style("top", `${e.pageY + 20}px`)
        .style("opacity", 1)
        .selectAll()
        .data([data])
        .enter()
        .append("div")
        .html((d, i) => {
          const currYear = d.filter((y) => y.Year === displayYear.toString());
          if (currYear.length === 0) {
            return null;
          }
          return `<div style="color: #137880;">${currYear[0].ScotlandPopulationNum}</div><div style="color: #5A7CD6;">${currYear[0].NorwayPopulationNum}</div>`;
        });
    };

    const handleMouseOut = () => {
      tooltipLine.attr("stroke", "none");
      tooltip.style("opacity", 0);
      if (tooltip) tooltip.style("opacity", 0);
      if (tooltipLine) tooltipLine.attr("stroke", "none");
    };

    const tooltipLine = chartArea.append("line");

    const tooltipContainer = chartArea
      .append("rect")
      .attr("width", innerWidth)
      .attr("height", innerHeight)
      .attr("fill", "none")
      .style("pointer-events", "all")
      .on("mousemove", handleMouseMove)
      .on("mouseout", handleMouseOut);
  }, [createXScale, createYScale, data, renderValueLinePath]);

  useEffect(() => {
    if (data) {
      renderChart();
    }
  }, [data, renderChart]);

  // No data!
  if (!data) {
    return (
      <div className="container">
        <pre>Loading...</pre>
      </div>
    );
  }

  return (
    <div className="container">
      <svg ref={svgRef}></svg>
    </div>
  );
};
