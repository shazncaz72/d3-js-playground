export const Marks = ({ data, xScale, yScale, innerHeight }) =>
  data.map((d) => (
    <rect
      className="mark"
      key={d.Year}
      x={xScale(d.Year)}
      y={yScale(d.PopulationNum)}
      width={xScale.bandwidth()}
      height={innerHeight - yScale(d.PopulationNum)}
    />
  ));
