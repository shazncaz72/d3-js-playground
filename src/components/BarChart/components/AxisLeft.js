import { format } from "d3";

export const AxisLeft = ({ yScale, innerWidth }) =>
  yScale.ticks().map((tickValue) => (
    <g
      className="tick"
      key={tickValue}
      transform={`translate(0, ${yScale(tickValue)})`}
    >
      <line x2={innerWidth} />
      <text style={{ textAnchor: "end" }} x={-10} dy={"0.35em"}>
        {format(",.0f")(tickValue)}
      </text>
    </g>
  ));
