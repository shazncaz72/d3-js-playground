export const AxisBottom = ({ xScale, innerHeight }) =>
  xScale.domain().map((tickValue) => (
    <g key={tickValue} className="tick">
      <text
        key={tickValue}
        x={xScale(tickValue)}
        y={innerHeight + 15}
        transform={`rotate(-65, ${xScale(tickValue) + xScale.bandwidth()}, ${
          innerHeight + 15
        })`}
        style={{ textAnchor: "middle" }}
      >
        {tickValue}
      </text>
    </g>
  ));
