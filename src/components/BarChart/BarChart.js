import React from "react";
import "../../App.css";
import { min, max, scaleBand, scaleLinear } from "d3";
import { useData } from "../../hooks/useData";
import { AxisBottom } from "./components/AxisBottom";
import { AxisLeft } from "./components/AxisLeft";
import { Marks } from "./components/Marks";

const width = 960;
const height = 500;
const margin = { top: 70, right: 50, bottom: 100, left: 140 };

const BarChart = () => {
  const data = useData();

  // No data!
  if (!data) {
    return <pre>Loading...</pre>;
  }

  // Margin convention
  const innerWidth = width - margin.left - margin.right;
  const innerHeight = height - margin.top - margin.bottom;

  const xScale = scaleBand()
    .domain(data.map((d) => d.Year))
    .range([0, innerWidth])
    .padding(0.2);

  const yScale = scaleLinear()
    .domain([
      min(data, (d) => d.PopulationNum),
      max(data, (d) => d.PopulationNum),
    ])
    .range([innerHeight, 0])
    .nice();

  return (
    <div className="container">
      <svg width={width} height={height}>
        <g transform={`translate(${margin.left},${margin.top})`}>
          <text className="d3-chart-title" x={innerWidth / 2} y={-20}>
            Scottish Population - 1972 to 2019
          </text>
          <text
            className="x-axis-label"
            x={innerWidth / 2}
            y={innerHeight + 70}
          >
            Year
          </text>
          <text
            className="y-axis-label"
            x={-95}
            y={innerHeight / 2}
            transform={`rotate(-90, ${-95}, ${innerHeight / 2})`}
          >
            Population
          </text>
          <AxisBottom xScale={xScale} innerHeight={innerHeight} />
          <AxisLeft yScale={yScale} innerWidth={innerWidth} />
          <Marks
            data={data}
            xScale={xScale}
            yScale={yScale}
            innerHeight={innerHeight}
          />
        </g>
      </svg>
    </div>
  );
};

export default BarChart;
