import React, { useCallback, useEffect, useRef } from "react";
import "../../App.css";
import { arc, interpolate, scaleLinear, select } from "d3";

export const AnimatedGauge = ({
  svgWidth = 220,
  svgHeight = 220,
  gaugeRadius = 100,
  value = 0,
  minValue = 0,
  maxValue = 100,
  strapLine,
  fill = "#F2B437",
  backgroundArcStrokeWidth = 4,
  foregroundArcStrokeWidth = 12,
}) => {
  const svgRef = useRef();

  const backgroundArc = arc()
    .innerRadius(gaugeRadius - backgroundArcStrokeWidth / 2)
    .outerRadius(gaugeRadius + backgroundArcStrokeWidth / 2)
    .startAngle(0); // 0 radians at 12 0'clock

  const foregroundArc = arc()
    .innerRadius(gaugeRadius - foregroundArcStrokeWidth / 2)
    .outerRadius(gaugeRadius + foregroundArcStrokeWidth / 2)
    .startAngle(Math.PI);

  const angleScale = scaleLinear()
    .domain([minValue, maxValue])
    .range([Math.PI, -Math.PI])
    .clamp(true);
  const angle = angleScale(value);

  const arcTween = useCallback(
    (newAngle) => {
      return function (d) {
        const interpolator = interpolate(d.endAngle, newAngle);

        return (t) => {
          d.endAngle = interpolator(t);
          return foregroundArc(d);
        };
      };
    },
    [foregroundArc]
  );

  useEffect(() => {
    const svg = select(svgRef.current);
    svg.attr("width", svgWidth).attr("height", svgHeight);

    const g = svg
      .append("g")
      .attr("transform", `translate(${svgWidth / 2}, ${svgHeight / 2})`);

    g.append("path")
      .datum({ endAngle: Math.PI * 2 }) // 6.27 radians back around at 12 0'clock
      .style("fill", fill)
      .attr("d", backgroundArc);

    const foreground = g
      .append("path")
      .datum({ endAngle: Math.PI })
      .style("fill", fill)
      .attr("d", foregroundArc);

    g.append("text").text(`${value}%`).attr("class", "metric");
    g.append("text").text(strapLine).attr("class", "sub-metric").attr("y", 35);

    foreground.transition().duration(2500).attrTween("d", arcTween(angle));
  }, [
    svgWidth,
    svgHeight,
    gaugeRadius,
    value,
    minValue,
    maxValue,
    strapLine,
    fill,
    backgroundArcStrokeWidth,
    foregroundArcStrokeWidth,
    backgroundArc,
    foregroundArc,
    angle,
    arcTween,
  ]);

  return <svg ref={svgRef}></svg>;
};
