import React from "react";
import "../../App.css";
import { arc, scaleLinear } from "d3";

export const Gauge = ({
  svgWidth = 220,
  svgHeight = 220,
  gaugeRadius = 100,
  value = 0,
  minValue = 0,
  maxValue = 100,
  strapLine,
  fill = "#F2B437",
  backgroundArcStrokeWidth = 4,
  foregroundArcStrokeWidth = 12,
}) => {
  const backgroundArc = arc()
    .innerRadius(gaugeRadius - backgroundArcStrokeWidth / 2)
    .outerRadius(gaugeRadius + backgroundArcStrokeWidth / 2)
    .startAngle(0) // 0 radians at 12 0'clock
    .endAngle(Math.PI * 2);

  const angleScale = scaleLinear()
    .domain([minValue, maxValue])
    .range([Math.PI, -Math.PI])
    .clamp(true);
  const angle = angleScale(value);

  const filledArc = arc()
    .innerRadius(gaugeRadius - foregroundArcStrokeWidth / 2)
    .outerRadius(gaugeRadius + foregroundArcStrokeWidth / 2)
    .startAngle(Math.PI)
    .endAngle(angle);

  return (
    <svg width={svgWidth} height={svgHeight}>
      <g transform={`translate(${svgWidth / 2}, ${svgHeight / 2})`}>
        <path d={backgroundArc()} fill={fill} />
        <path d={filledArc()} fill={fill} />
        <text className="metric">{value}%</text>
        <text className="sub-metric" y={30}>
          {strapLine}
        </text>
      </g>
    </svg>
  );
};
