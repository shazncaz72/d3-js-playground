import React from "react";
import { Gauge } from "./Gauge";

// Gauge
const svgWidth = 220;
const svgHeight = 220;
const gaugeRadius = 100;
const value = 90;
const minValue = 0;
const maxValue = 100;
const backgroundArcStrokeWidth = 4;
const foregroundArcStrokeWidth = 12;

const GaugeContainer = () => (
  <div className="container">
    <Gauge
      svgWidth={svgWidth}
      svgHeight={svgHeight}
      gaugeRadius={gaugeRadius}
      value={value}
      minValue={minValue}
      maxValue={maxValue}
      strapLine={"1.4.7"}
      fill="#F2B437"
      backgroundArcStrokeWidth={backgroundArcStrokeWidth}
      foregroundArcStrokeWidth={foregroundArcStrokeWidth}
    />
    <Gauge
      svgWidth={svgWidth}
      svgHeight={svgHeight}
      gaugeRadius={gaugeRadius}
      value={3.4}
      minValue={minValue}
      maxValue={maxValue}
      strapLine={"1.3.7"}
      fill="#A5B7E5"
      backgroundArcStrokeWidth={backgroundArcStrokeWidth}
      foregroundArcStrokeWidth={foregroundArcStrokeWidth}
    />
    <Gauge
      svgWidth={svgWidth}
      svgHeight={svgHeight}
      gaugeRadius={gaugeRadius}
      value={1.6}
      minValue={minValue}
      maxValue={maxValue}
      strapLine={"1.2.7"}
      fill="#888888"
      backgroundArcStrokeWidth={backgroundArcStrokeWidth}
      foregroundArcStrokeWidth={foregroundArcStrokeWidth}
    />
  </div>
);

export default GaugeContainer;
