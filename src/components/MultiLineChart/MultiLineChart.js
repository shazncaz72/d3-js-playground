import React, { useCallback, useEffect, useRef } from "react";
import { useData } from "../../hooks/useData";
import {
  axisBottom,
  axisLeft,
  easeSinInOut,
  extent,
  format,
  line,
  max,
  scaleTime,
  scaleLinear,
  select,
} from "d3";

const width = 960;
const height = 500;
// Margin convention
const margin = { top: 60, right: 50, bottom: 80, left: 120 };
const innerWidth = width - margin.left - margin.right;
const innerHeight = height - margin.top - margin.bottom;

// Tooltip
const tooltip = select("body")
  .append("div")
  .attr("class", "tooltip")
  .style("position", "absolute")
  .style("opacity", 0);

const tooltipPosnX = (pageX) => {
  if (pageX <= innerWidth) {
    return pageX + 10;
  }
  return pageX - 75 - 10;
};

export const MultiLineChart = () => {
  const svgRef = useRef();

  const data = useData(true);

  const createXScale = useCallback(() => {
    return scaleTime()
      .domain(extent(data, (d) => d.ParsedYear))
      .range([0, innerWidth])
      .nice();
  }, [data]);

  const createYScale = useCallback(() => {
    return scaleLinear()
      .domain([
        0,
        max(data, (d) =>
          Math.max(d.ScotlandPopulationNum, d.NorwayPopulationNum)
        ),
      ])
      .range([innerHeight, 0])
      .nice();
  }, [data]);

  const renderXAxis = (svg, xScale) => {
    const xAxis = axisBottom(xScale);

    svg
      .select(".x-axis")
      .style("transform", `translateY(${innerHeight}px)`)
      .call(xAxis)
      .selectAll("text")
      .attr("dy", "1.4em");
  };

  const renderYAxis = (svg, yScale) => {
    const yAxis = axisLeft(yScale);
    svg
      .select(".y-axis")
      .call(yAxis)
      .selectAll("line")
      .attr("x1", -6)
      .attr("x2", 8);
  };

  const renderAxisLabels = (chartArea, xAxisLabel, yAxisLabel) => {
    // X axis label
    chartArea
      .append("text")
      .attr("class", "x-axis-label")
      .attr("x", innerWidth / 2)
      .attr("y", innerHeight + 60)
      .text(xAxisLabel);

    // Y axis label
    chartArea
      .append("text")
      .attr("class", "y-axis-label")
      .attr("x", -90)
      .attr("y", innerHeight / 2)
      .attr("transform", `rotate(-90, ${-90}, ${innerHeight / 2})`)
      .text(yAxisLabel);
  };

  const renderYAxisGridLines = (chartArea, yScale) => {
    chartArea
      .selectAll("line.y-axis-grid")
      .data(yScale.ticks())
      .enter()
      .append("line")
      .attr("class", "y-axis-grid")
      .attr("x1", 18)
      .attr("x2", innerWidth)
      .attr("y1", (d) => yScale(d) + 1)
      .attr("y2", (d) => yScale(d) + 1)
      .attr("stroke", (d, idx) => {
        if (idx === 0) {
          return "none";
        }
        return "#f0f0f0";
      });
  };

  const animatePath = (path) => {
    const totalPathLength = path.node().getTotalLength();
    path
      .attr("stroke-dasharray", `${totalPathLength} ${totalPathLength}`)
      .attr("stroke-dashoffset", path.node().getTotalLength())
      .transition()
      .duration(3000)
      .ease(easeSinInOut)
      .attr("stroke-dashoffset", 0);
  };

  const renderValueLinePath = useCallback(
    (chartArea, className, valueLine, stroke) => {
      return chartArea
        .selectAll(`.${className}`)
        .data([data])
        .join("path")
        .attr("class", className)
        .attr("d", (value) => valueLine(value))
        .attr("fill", "none")
        .attr("stroke", stroke)
        .attr("stroke-width", 2);
    },
    [data]
  );

  const renderMarkers = useCallback(
    (chartArea, xScale, yScale, className, fill, valueKey) => {
      chartArea
        .selectAll(`.${className}`)
        .data(data)
        .enter()
        .append("circle")
        .attr("class", className)
        .attr("opacity", 0)
        .attr("r", 5)
        .attr("cx", (value) => xScale(value.ParsedYear))
        .attr("cy", (value) => yScale(value[valueKey]))
        .attr("fill", fill)
        .on("mouseover", (d, value) => {
          tooltip
            .html(`${value.Year}<br/>${format(",d")(value[valueKey])}`)
            .style("left", `${tooltipPosnX(d.pageX)}px`)
            .style("top", `${d.pageY - 44 - 10}px`)
            .style("background-color", fill)
            .transition()
            .duration(200)
            .style("opacity", 1);
        })
        .on("mouseout", (d) => {
          tooltip.transition().duration(300).style("opacity", 0);
        })
        .transition()
        .delay(3000)
        .duration(300)
        .attr("opacity", 1);
    },
    [data]
  );

  const renderChart = useCallback(() => {
    const svg = select(svgRef.current);
    svg.attr("width", width).attr("height", height);

    // Margin convention
    const chartArea = svg
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);
    chartArea.append("g").attr("class", "x-axis axis");
    chartArea.append("g").attr("class", "y-axis axis");

    const xScale = createXScale();
    const yScale = createYScale();

    renderXAxis(svg, xScale);
    renderYAxis(svg, yScale);

    renderAxisLabels(chartArea, "Year", "Population");

    renderYAxisGridLines(chartArea, yScale);

    const valueLineScotland = line()
      .x((value) => xScale(value.ParsedYear))
      .y((value) => yScale(value.ScotlandPopulationNum));

    const valueLineNorway = line()
      .x((value) => xScale(value.ParsedYear))
      .y((value) => yScale(value.NorwayPopulationNum));

    // Path for line
    const pathScotland = renderValueLinePath(
      chartArea,
      "line-scotland",
      valueLineScotland,
      "#137880"
    );

    const pathNorway = renderValueLinePath(
      chartArea,
      "line-norway",
      valueLineNorway,
      "#5A7CD6"
    );

    // Animate
    animatePath(pathScotland);
    animatePath(pathNorway);

    // Markers
    renderMarkers(
      chartArea,
      xScale,
      yScale,
      "marker-scotland",
      "#137880",
      "ScotlandPopulationNum"
    );

    renderMarkers(
      chartArea,
      xScale,
      yScale,
      "marker-norway",
      "#5A7CD6",
      "NorwayPopulationNum"
    );
  }, [createXScale, createYScale, renderMarkers, renderValueLinePath]);

  useEffect(() => {
    if (data) {
      renderChart();
    }
  }, [data, renderChart]);

  // No data!
  if (!data) {
    return (
      <div className="container">
        <pre>Loading...</pre>
      </div>
    );
  }

  return (
    <div className="container">
      <svg ref={svgRef}></svg>
    </div>
  );
};
